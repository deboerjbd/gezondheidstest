/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hva.jeffrey;

import java.util.Scanner;

/**
 *
 * @author Jeffrey Jeffrey.de.Boer2@hva.nl
 * script om je gezondheid te meten op basis van ingevoerde hartslag, 
 * lichaamstemperatuur en bovendruk
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //declareer scanner.
        Scanner input = new Scanner(System.in);
        
        //declareer minimum en maximum values.
        double minHartslag = 55;
        double maxHartslag = 90;
        double minTemp = 36.3;
        double maxTemp = 37.5;
        double minBovendruk = 100;
        double maxBovendruk = 140;
        
        //invoer voor hartslag.
        System.out.print("wat is uw hartslag (slagen per minuut): ");
        double hartslag = input.nextDouble();
        
        //invoer voor lichaamstemperatuur.
        System.out.print("Wat is uw lichaamstemperatuur (graden Celsius): ");
        double lichaamsTemp = input.nextDouble();
        
        //invoer voor bovendruk.
        System.out.print("Wat is uw bovendruk: (mm Hg)");
        double bovendruk = input.nextDouble();
       
        
       // IF Statements om gezond of ongezond uit te printen.
        
       if (tussen(hartslag,minHartslag,maxHartslag)) {
           System.out.println("Uw hartslag is gezond.");
       } else 
            System.out.println("Uw hartslag is ongezond.");

       if (tussen(lichaamsTemp,minTemp,maxTemp)) {
           System.out.println("Uw lichaamstemperatuur is gezond.");
       } else
            System.out.println("Uw lichaamstemperatuur is ongezond.");

       if (tussen(bovendruk,minBovendruk,maxBovendruk)) {
           System.out.println("Uw bovendruk is gezond.");
       } else
            System.out.println("Uw bovendruk is ongezond.");
    
    }
    
    //Method die de tussen berekend en er een boolean false voor terug geeft.
    static boolean tussen(double waarde, double min, double max) {
        if (min <=waarde && waarde <=max) {      
        return true;
        } else {
            return false;
        }
    
    } 
    
}
